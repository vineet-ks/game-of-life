#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include "../src/game_of_life.h"

TEST_CASE("Check rules")
{
    unsigned int rows{20};
    unsigned int columns{40};

    CellGrid current_state(rows, std::vector<bool>(columns, false));
    CellGrid previous_state(rows, std::vector<bool>(columns, false));
    CountGrid neighbour_count(rows, std::vector<int>(columns, 0));

    get_initial_state(current_state);
    get_neighbour_count(current_state, neighbour_count);
    apply_rules(current_state, neighbour_count);

    CellGrid state(rows, std::vector<bool>(columns, false));
    state[10][15] = true;
    state[11][15] = true;
    state[10][16] = true;
    state[11][16] = true;

    CHECK(current_state == state);
}
