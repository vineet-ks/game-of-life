#include "logic.h"

#include <iostream>

namespace Logic
{

void toggle_cell(CellGrid &state, GridCoordinates coords)
{
    state[coords.y][coords.x] = !state[coords.y][coords.x];
}

void draw(CellGrid &state)
{
    for (auto row : state)
    {
        for (auto element : row)
        {
            if (element == true)
                std::cout << 'x' << ' ';
            else
                std::cout << '.' << ' ';
        }
        std::cout << '\n';
    }
}

void draw_neighbour(CountGrid &state)
{
    for (auto row : state)
    {
        for (auto element : row)
        {
            std::cout << element;
        }
        std::cout << '\n';
    }
}

void get_neighbour_count(CellGrid &state, CountGrid &neighbour_count)
{
    std::size_t rows = neighbour_count.size();
    std::size_t columns = neighbour_count[0].size();
    constexpr std::size_t O{0};

    for (std::size_t i{0}; i < rows; ++i)
    {
        for (std::size_t j{0}; j < columns; ++j)
        {
            int count{0};
            for (auto offset_i{std::max(O, i - 1)}; offset_i < std::min(i + 2, rows); ++offset_i)
            {
                for (auto offset_j{std::max(O, j - 1)}; offset_j < std::min(j + 2, columns); ++offset_j)
                {
                    if (state[offset_i][offset_j] == true)
                        ++count;
                }
            }
            neighbour_count[i][j] = (state[i][j] == false) ? count : --count;
        }
    }
}

void apply_rules(CellGrid &current_state, CountGrid &neighbour_count, std::vector<GridCoordinates> &coords_toggle_alive,
                 std::vector<GridCoordinates> &coords_toggle_dead)
{

    std::size_t rows = neighbour_count.size();
    std::size_t columns = neighbour_count[0].size();
    for (std::size_t i{0}; i < rows; ++i)
    {
        for (std::size_t j{0}; j < columns; ++j)
        {
            if (current_state[i][j] == false)
            {
                if (neighbour_count[i][j] == 3)
                {
                    current_state[i][j] = true;
                    coords_toggle_alive.push_back(
                        GridCoordinates{static_cast<unsigned int>(j), static_cast<unsigned int>(i)});
                }
            }

            else
            {
                switch (neighbour_count[i][j])
                {
                case 2:
                case 3:
                    break;

                default:
                    current_state[i][j] = false;
                    coords_toggle_dead.push_back(
                        GridCoordinates{static_cast<unsigned int>(j), static_cast<unsigned int>(i)});
                    break;
                }
            }
        }
    }
}

} // namespace Logic
