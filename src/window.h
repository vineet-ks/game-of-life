#include <SFML/Graphics.hpp>
#include <array>
#include <vector>

#ifndef GOL_GRIDCOORD
#define GOL_GRIDCOORD

struct GridCoordinates
{
    unsigned int x;
    unsigned int y;
};

#endif

struct Dimensions
{
    unsigned int rows;
    unsigned int columns;
};

enum class ClickRegion
{
    grid,
    menu,
    invalid
};

class RenderGame
{
    const unsigned int spacing;
    const unsigned int border_width;
    const sf::Color bg_colour;
    const sf::Color grid_colour;
    const sf::Color cell_colour;
    unsigned int n_h_lines;
    unsigned int n_v_lines;
    sf::Font font;

  public:
    RenderGame(const unsigned int, const unsigned int, const sf::Color, const sf::Color, const sf::Color);
    Dimensions calculate_grid(sf::RenderWindow &);
    void draw_grid(sf::RenderWindow &);
    GridCoordinates get_grid_coordinates(sf::Vector2i);
    void toggle_cell(sf::RenderWindow &, GridCoordinates, bool);
    ClickRegion check_region(sf::Vector2i);
    void apply_rules(sf::RenderWindow &, std::vector<GridCoordinates> &, std::vector<GridCoordinates> &);
    void set_font();
    void draw_menu(sf::RenderWindow &);
};
