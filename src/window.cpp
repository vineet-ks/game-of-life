#include "window.h"

#include <iostream>
//#include <typeinfo>

RenderGame::RenderGame(const unsigned int border_width, const unsigned int spacing, const sf::Color bg_colour,
                       const sf::Color grid_colour, const sf::Color cell_colour)
    : spacing{spacing}, border_width{border_width}, bg_colour{bg_colour}, grid_colour{grid_colour},
      cell_colour{cell_colour}, n_h_lines{}, n_v_lines{}, font{}
{
}

Dimensions RenderGame::calculate_grid(sf::RenderWindow &window)
{
    sf::Vector2u window_size{window.getSize()};
    sf::FloatRect visibleArea(0.0, 0.0, static_cast<float>(window_size.x), static_cast<float>(window_size.y));
    window.setView(sf::View(visibleArea));

    n_h_lines = (window_size.y - 2 * border_width) / spacing + 1;
    n_v_lines = (window_size.x - 2 * border_width) / spacing + 1;

    window.clear();

    return {n_h_lines - 1, n_v_lines - 1};
}

void RenderGame::draw_grid(sf::RenderWindow &window)
{
    std::array<sf::Vertex, 2> v_line;
    std::array<sf::Vertex, 2> h_line;

    for (unsigned int i{0}; i < n_v_lines; ++i)
    {
        v_line = {
            sf::Vertex(sf::Vector2f(static_cast<float>(border_width + i * spacing), static_cast<float>(border_width)),
                       grid_colour),
            sf::Vertex(sf::Vector2f(static_cast<float>(border_width + i * spacing),
                                    static_cast<float>(border_width + (n_h_lines - 1) * spacing)),
                       grid_colour)};

        window.draw(&v_line[0], 2, sf::Lines);
    }

    for (unsigned int i{0}; i < n_h_lines; ++i)
    {
        h_line = {
            sf::Vertex(sf::Vector2f(static_cast<float>(border_width), static_cast<float>(border_width + i * spacing)),
                       grid_colour),
            sf::Vertex(sf::Vector2f(static_cast<float>(border_width + (n_v_lines - 1) * spacing),
                                    static_cast<float>(border_width + i * spacing)),
                       grid_colour)};

        window.draw(&h_line[0], 2, sf::Lines);
    }
}

ClickRegion RenderGame::check_region(sf::Vector2i mouse_position)
{
    if (mouse_position.x > static_cast<int>(border_width) &&
        mouse_position.x < static_cast<int>(border_width + (n_v_lines - 1) * spacing) &&
        mouse_position.y > static_cast<int>(border_width) &&
        mouse_position.y < static_cast<int>(border_width + (n_h_lines - 1) * spacing))
    {
        return ClickRegion::grid;
    }
    else
    {
        return ClickRegion::invalid;
    }
}

GridCoordinates RenderGame::get_grid_coordinates(sf::Vector2i mouse_position)
{
    int x_grid_coord{(mouse_position.x - static_cast<int>(border_width)) / static_cast<int>(spacing)};
    int y_grid_coord{(mouse_position.y - static_cast<int>(border_width)) / static_cast<int>(spacing)};

    return GridCoordinates{static_cast<unsigned int>(x_grid_coord), static_cast<unsigned int>(y_grid_coord)};
}

void RenderGame::toggle_cell(sf::RenderWindow &window, GridCoordinates coords, bool cell_state)
{

    unsigned int x_left_window_cell_vertex{border_width + coords.x * spacing};
    unsigned int y_upper_window_cell_vertex{border_width + coords.y * spacing};
    sf::RectangleShape cell_square(sf::Vector2f(static_cast<float>(spacing - 1), static_cast<float>(spacing - 1)));
    cell_square.setPosition(static_cast<float>(x_left_window_cell_vertex),
                            static_cast<float>(y_upper_window_cell_vertex + 1));
    sf::Color fill_colour = (cell_state == false) ? cell_colour : bg_colour;
    cell_square.setFillColor(fill_colour);

    window.draw(cell_square);
}

void RenderGame::apply_rules(sf::RenderWindow &window, std::vector<GridCoordinates> &coords_toggle_alive,
                             std::vector<GridCoordinates> &coords_toggle_dead)
{
    for (GridCoordinates coords : coords_toggle_alive)
    {
        toggle_cell(window, coords, false);
    }

    for (GridCoordinates coords : coords_toggle_dead)
    {
        toggle_cell(window, coords, true);
    }
}

void RenderGame::set_font()
{
    if (!font.loadFromFile("../fonts/24LedBright-XElj.ttf"))
    {
        std::cerr << "Font cannot be loaded";
    }

    font.loadFromFile("../fonts/24LedBright-XElj.ttf");
}

void RenderGame::draw_menu(sf::RenderWindow &window)
{
    sf::Text text;

    text.setFont(font);
    text.setOrigin(-650, 0);
    text.setString("Game of Life");
    text.setCharacterSize(100);
    text.setFillColor(sf::Color{185, 201, 189});
    window.draw(text);

    text.setString("Rules:\n\n"
                   "1. Any live cell with fewer than two live neighbors dies, as if by underpopulation.\n"
                   "2. Any live cell with two or three live neighbors lives on to the next generation.\n"
                   "3. Any live cell with more than three live neighbors dies, as if by overpopulation.\n"
                   "4. Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.");
    text.setOrigin(-150, -200);
    text.setCharacterSize(40);
    window.draw(text);

    text.setString("S Start");
    text.setOrigin(-150, -800);
    text.setCharacterSize(80);
    window.draw(text);

    text.setString("P Play/Pause");
    text.setOrigin(-700, -800);
    window.draw(text);

    text.setString("M Menu");
    text.setOrigin(-1500, -800);
    window.draw(text);
}
