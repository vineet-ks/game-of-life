#include <vector>

using CellGrid = std::vector<std::vector<bool>>;
using CountGrid = std::vector<std::vector<int>>;

#ifndef GOL_GRIDCOORD
#define GOL_GRIDCOORD

struct GridCoordinates
{
    unsigned int x;
    unsigned int y;
};

#endif

namespace Logic
{

void get_neighbour_count(CellGrid &, CountGrid &);

void draw_neighbour(CountGrid &);

void draw(CellGrid &);

void apply_rules(CellGrid &, CountGrid &, std::vector<GridCoordinates> &, std::vector<GridCoordinates> &);
void toggle_cell(CellGrid &, GridCoordinates);

} // namespace Logic
