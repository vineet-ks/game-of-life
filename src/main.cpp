#include "logic.h"
#include "window.h"

enum class GameMode
{
    menu,
    play,
    pause
};

void trigger_next_state(const Dimensions dims, CellGrid &current_state,
                        std::vector<GridCoordinates> &coords_toggle_alive,
                        std::vector<GridCoordinates> &coords_toggle_dead, RenderGame &game_window,
                        sf::RenderWindow &window)
{

    CountGrid neighbour_count(dims.rows, std::vector<int>(dims.columns, 0));
    Logic::get_neighbour_count(current_state, neighbour_count);
    Logic::apply_rules(current_state, neighbour_count, coords_toggle_alive, coords_toggle_dead);
    game_window.apply_rules(window, coords_toggle_alive, coords_toggle_dead);
}

int main()
{
    constexpr unsigned int spacing{20};
    constexpr unsigned int border_width{60};
    const sf::Color bg_colour{sf::Color::Black};
    const sf::Color cell_colour{90, 5, 0};
    const sf::Color grid_colour{170, 170, 170};
    const sf::Time state_interval_time{sf::milliseconds(200)};

    sf::RenderWindow window(sf::VideoMode(1920, 1080), "Game of Life");
    RenderGame game_window(border_width, spacing, bg_colour, grid_colour, cell_colour);
    CellGrid current_state;
    GridCoordinates coords;
    ClickRegion region;
    Dimensions dims;
    sf::Vector2i mouse_position;
    bool cell_state;
    std::vector<GridCoordinates> coords_toggle_alive;
    std::vector<GridCoordinates> coords_toggle_dead;
    sf::Clock clock;
    sf::Time elapsed_interval_time;

    bool draw_menu{true};
    GameMode mode{GameMode::menu};
    game_window.set_font();
    clock.restart();

    while (window.isOpen())
    {
        sf::Event event;

        if (mode == GameMode::play)
        {
            if (clock.getElapsedTime() >= state_interval_time)
            {
                trigger_next_state(dims, current_state, coords_toggle_alive, coords_toggle_dead, game_window, window);
                window.display();
                coords_toggle_alive.clear();
                coords_toggle_dead.clear();
                clock.restart();
            }
        }

        while (window.pollEvent(event))
        {
            if (mode == GameMode::menu)
            {
                if (draw_menu == true)
                {
                    game_window.draw_menu(window);
                    draw_menu = false;
                    window.display();
                }

                switch (event.type)
                {
                default:
                    break;

                case (sf::Event::Closed):
                    window.close();
                    break;

                case (sf::Event::KeyPressed):

                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
                    {
                        mode = GameMode::pause;
                        draw_menu = true;

                        window.clear();
                        dims = game_window.calculate_grid(window);
                        game_window.draw_grid(window);
                        current_state.resize(dims.rows, std::vector<bool>(dims.columns));
                        current_state.assign(dims.rows, std::vector<bool>(dims.columns, false));
                        window.display();
                    }
                    break;

                case (sf::Event::Resized):

                    window.clear();
                    game_window.draw_menu(window);
                    window.display();
                    break;
                }
            }

            else
            {
                switch (event.type)
                {
                default:
                    break;

                case (sf::Event::Closed):
                    window.close();
                    break;

                case (sf::Event::Resized):

                    dims = game_window.calculate_grid(window);
                    game_window.draw_grid(window);
                    current_state.resize(dims.rows, std::vector<bool>(dims.columns));
                    current_state.assign(dims.rows, std::vector<bool>(dims.columns, false));
                    window.display();
                    break;

                case (sf::Event::MouseButtonPressed):

                    if (event.mouseButton.button == sf::Mouse::Left)
                    {
                        mouse_position = sf::Mouse::getPosition(window);
                        region = game_window.check_region(mouse_position);
                        if (region == ClickRegion::grid)
                        {
                            coords = game_window.get_grid_coordinates(mouse_position);
                            cell_state = current_state[coords.y][coords.x];
                            game_window.toggle_cell(window, coords, cell_state);
                            window.display();
                            Logic::toggle_cell(current_state, coords);
                        }
                    }
                    break;

                case (sf::Event::KeyPressed):

                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
                    {
                        if (mode == GameMode::pause)
                        {
                            mode = GameMode::play;
                        }

                        else if (mode == GameMode::play)
                        {
                            mode = GameMode::pause;
                        }
                    }

                    if (sf::Keyboard::isKeyPressed(sf::Keyboard::M))
                    {
                        window.clear();
                        mode = GameMode::menu;
                    }
                    break;
                }

                if (mode == GameMode::play)
                {
                    if (clock.getElapsedTime() >= state_interval_time)
                    {

                        trigger_next_state(dims, current_state, coords_toggle_alive, coords_toggle_dead, game_window,
                                           window);
                        window.display();
                        coords_toggle_alive.clear();
                        coords_toggle_dead.clear();
                        clock.restart();
                    }
                }
            }
        }
    }
}
