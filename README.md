## Game of Life

Implementation of Conway's Game of Life with C++ and SFML
![game-of-life.gif](https://gitlab.com/vineet-ks/game-of-life/-/blob/master/game-of-life.gif?ref_type=heads)

### How to build

Make sure you have installed SFML dev library.
In fedora this can be done by the following command:

```
sudo dnf install SFML-devel
```

In debian based systems like ubuntu, this can be done by:

```
sudo apt install libsfml-dev
```

Clone the repository

```
git clone https://gitlab.com/vineet-ks/game-of-life.git
```

Go to the directory and make folders

```
cd game-of-life
mkdir build && cd build
```

Build

```
cmake ..
make
```

Run the program

```
./game-of-life
```

